/**
 * 
 */

//http://192.168.1.6:8081/unamelist
//function wlEnvInit(){

//wlCommonInit();
var userkey ="none";

$('#login').bind('click', function() {
	if(loginvalidator()){
		var invocationData = {
		    adapter : 'RestletAdapter',
		    procedure : 'getStories',
		    parameters : [$('#username').val(),$('#password').val()]
		};
		
    WL.Client.invokeProcedure(invocationData,{
        onSuccess : loadMenu
        //onFailure : mobGmapLatLngFailure,
    });
	}
});
function loadMenu(result) {
	
    var httpStatusCode = result.status;
    if (200 == httpStatusCode) {
    	
       var invocationResult = result.invocationResult;
       var returntext = invocationResult.text;
       
       if(returntext == "failed"){
    	   alert("please check your entry... try again");
       }else{
    	   userkey = result.invocationResult.text;
    	   $.mobile.navigate( "#pagemenu", { transition : "slide", info: "info about the #pagemenu " });
       }    
    } 
    else {
        alert("Error. httpStatusCode=" + httpStatusCode);
    }
}


$('#register').bind('click', function() {

	if(	inputvalidator($( '#fname').val(), $( '#lname' ).val(),$( '#email' ).val(), $( '#phone' ).val()) &&	
			passwordvalidator($('#pass' ).val(),$( '#cpass').val()) &&
			unamevalidator( $( '#uname' ).val())){
		var param = [ $('#fname').val(),
		              $('#lname').val(),
		              $('#uname').val(),
		              $('#pass').val(),
		              $('#email').val(),
		              $('#phone').val()];
		var invocationData = {
		    adapter : 'RestletAdapter',
		    procedure : 'addUser',
		    parameters : param
	
		};
		
    WL.Client.invokeProcedure(invocationData,{
        onSuccess : loadMenu
        //onFailure : mobGmapLatLngFailure,
    });
}
});

function loginvalidator(){
	var valid = true;
	if($('#username').val().trim().length ==0 ||
            $('#password').val().trim().length==0){
		valid = false;
		alert("Enter some values for the input field");	
	}
	return valid;
}
function passwordvalidator(pass,cpass){
	var valid = true;
	if(pass.trim().length==0){
		valid = false;
		alert("Enter some values for the password field");		
	}else if(pass != cpass){
		valid = false;
		alert("PASSWORD and CONFIRMPASSWORD should be same");
	}
	return valid;
}

function inputvalidator(fname,lname,email,phone){
	var valid = true;
	if(fname.trim().length ==0 ||
           lname.trim().length==0 ||
           email.trim().length==0 ||
           phone.trim().length==0 ){
		valid = false;
		alert("Enter some values for the input field");		
	}else if( email.indexOf('@') == -1){
		valid = false;
		alert("Enter some valid email");		
	}else if(phone.match(/^\d+$/)==null){
		valid = false;
		alert("Enter some valid phone number");
	}
	return valid;
}
function unamevalidator(uname){
	var valid = true;
	if(uname.trim().length==0 ){
		valid = false;
		alert("Enter some values for the user name field");		
	}
	return valid;
}
function currentpassvalidator(curpass){
	var valid = true;
	if(curpass.trim().length==0 ){
		valid = false;
		alert("Enter some values for the current password field");		
	}
	return valid;
}



$('#profileload').bind('click', function() {
	//alert(userkey);
		var invocationData = {
		    adapter : 'RestletAdapter',
		    procedure : 'getProfile',
		    parameters : [userkey]
		};
		
    WL.Client.invokeProcedure(invocationData,{
        onSuccess : loadProfile
        //onFailure : mobGmapLatLngFailure,
    });

});

function loadProfile(result) {
//alert(result.invocationResult.email);	
    var httpStatusCode = result.status;
    if (200 == httpStatusCode) {
    	
       var invocationResult = result.invocationResult;
       var returntext = invocationResult.text;
       
       if(returntext == "failed"){
    	   alert("Can't load profile... try again");
       }else{
    	   $('#profilefname').val(result.invocationResult.firstname);
    	   $('#profilelname').val(result.invocationResult.lastname);
    	   $('#profileemail').val(result.invocationResult.email);
    	   $('#profilephone').val(result.invocationResult.phone);
    	   
    	   $("#profilefname").attr("disabled","disabled") ;
   		   $("#profilelname").attr("disabled","disabled");
   		   $("#profileemail").attr("disabled","disabled");
   		   $("#profilephone").attr("disabled","disabled");
   		   $('#editprofile').show();
   		   $('#updateprofile').hide();
   		   
    	   $.mobile.navigate( "#pageprofile", { transition : "slide", info: "info about the #pageprofile " });
       }    
    } 
    else {
        alert("Error. httpStatusCode=" + httpStatusCode);
    }
}


$('#editprofile').bind('click', function() {
		//alert();
		
		$("#profilefname").textinput('enable');
		$("#profilelname").textinput('enable');
		$("#profileemail").textinput('enable');
		$("#profilephone").textinput('enable');
		$('#editprofile').hide();
		$('#updateprofile').show();
});



$('#updateprofile').bind('click', function() {
	if(inputvalidator($( '#profilefname').val(), $( '#profilelname' ).val(),$( '#profileemail' ).val(), $( '#profilephone' ).val())){
		var param = [ $('#profilefname').val(),
	              $('#profilelname').val(),
	              $('#profileemail').val(),
	              $('#profilephone').val(),
	              userkey,'profileupdate'];
		var invocationData = {
		    adapter : 'RestletAdapter',
		    procedure : 'updateProfile',
		    parameters : param
		};
		
		WL.Client.invokeProcedure(invocationData,{
        onSuccess : loadProfile
        //onFailure : mobGmapLatLngFailure,
    });
	}

});
$('#changepass').bind('click', function() {
	if(passwordvalidator($('#profilenewpass' ).val(),$( '#profileconfpass').val()) &&
			currentpassvalidator($('#profilecurpass' ).val())){
		
		var param = [ $('#profilecurpass').val(),
		              $('#profilenewpass').val(),
		              userkey,'changepass'];
			var invocationData = {
			    adapter : 'RestletAdapter',
			    procedure : 'changePassword',
			    parameters : param
			};
			
			WL.Client.invokeProcedure(invocationData,{
	        onSuccess : loadSettings 
	        //onFailure : mobGmapLatLngFailure,
	    });
	}
});

function loadSettings(result) {
	//alert(result.invocationResult.email);	
	    var httpStatusCode = result.status;
	    if (200 == httpStatusCode) {
	    	
	       var invocationResult = result.invocationResult;
	       var returntext = invocationResult.text;
	       
	       if(returntext == "failed"){
	    	   alert("Change Password failed... try again");
	       }else{
	   		   alert("Password change successful");
	    	   $.mobile.navigate( "#pagesettings", { transition : "slide", info: "info about the #pagesettings " });
	       }    
	    } 
	    else {
	        alert("Error. httpStatusCode=" + httpStatusCode);
	    }
	}
//$('#logout').bind('click', function() {
	//WL.Client.logout();
//});

$('#loadmainservices').bind('click', callloadmainservices);
$('#radio1').bind('click', setfService);
$('#radio2').bind('click', setmService);

var serviceparam;
function callloadmainservices() {
	
	serviceparam = $("#radio-button :radio:checked").val();
	
	var invocationData = {
	    adapter : 'RestletAdapter',
	    procedure : 'loadmainservices',
	    parameters : [serviceparam]
	};
	
	WL.Client.invokeProcedure(invocationData,{
    onSuccess : loadServices
    //onFailure : mobGmapLatLngFailure,
});
}
var fservices;
var mservices;
var resultSet;
function loadServices(result) {
	//alert(serviceparam);

	    var httpStatusCode = result.status;
	    if (200 == httpStatusCode) {
	    	
	       var invocationResult = result.invocationResult;
	       var success = invocationResult.isSuccessful;
	       
	       if(success != true){
	    	   alert("Failed... try again");
	       }else{
	    	   if(serviceparam == 'female'){
	    		   fservices = invocationResult;
	    	   }else{
	    		   mservices = invocationResult;
	    		   }
	    	   $.mobile.navigate( "#pageservices", { transition : "slide", info: "info about the #pageservices " });
	       }    
	    } 
	    else {
	        alert("Error. httpStatusCode=" + httpStatusCode);
	    }
	}
$('#servicesbut').bind('click', setPageServices);
function  setPageServices(){
	   if(serviceparam == 'female'){
		 setfService();
	   }else{
		   setmService();
		   }
}

function loopmainService(){
	 var divservicesmain = $("#maindiv");
		var servcount =  resultSet.length;
		var i;
	
		for(i = 0; i <= servcount; i++){
			divservicesmain.append(' <a id="mainservicesbut'+i+'" data-role="button"   data-icon="arrow-d" data-iconpos="right"  style =" height: 15px" '+
					'onclick="loadDetailedService('+i+')">'+
					resultSet[i][0].mainservice +
					'</a>').trigger( "create" );
			
		}
		
		divservicesmain.append(	' </div>');
		 
}
function  setfService(){
	emptydiv();
	if(fservices == null){
		callloadmainservices();
	}else{
		 resultSet = fservices.result;
		 loopmainService();
		
	}
}


function  setmService(){
	emptydiv();
	if(mservices == null){
		callloadmainservices();
	}
	else{
		 resultSet = mservices.result;
		 loopmainService();
		
	}
}

function loadDetailedService(data){

	emptydiv();
	  var datalength = resultSet[data].length;
	  var i;
	 
	  var divservicesmain = $("#maindiv");
	  divservicesmain.append('<a href="#" style =" background: pink"  data-role="button"   data-icon="arrow-d" data-iconpos="right">'+resultSet[data][0].mainservice+'</a>');
	  divservicesmain.append('<table data-role="table" id="myservicestable">');
	  divservicesmain.append(' <tbody>');
	  for(i=0; i <datalength ; i ++){
		  divservicesmain.append(' <tr>');
		  divservicesmain.append('<td>'+resultSet[data][i].servicename+'</td>');
		  divservicesmain.append(' </tr><tr>');
		  divservicesmain.append('<td><img border="0" src="images/timer.jpg" width="20" height="20">'+resultSet[data][i].servicetime+'</td>');
		  divservicesmain.append('<td><img border="0" src="images/dollar.jpg" width="20" height="20">'+resultSet[data][i].serviceamount+'</td>');
		  divservicesmain.append('<td><img border="0" src="images/cart.jpg" width="20" height="20"><input type="checkbox" /></td>');
		  divservicesmain.append(' </tr>');
	  }
	 
	  divservicesmain.append(' </tbody>  </table>');

	}

function emptydiv(){
	 $('#maindiv').empty();
}




